using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace WhereUsedPlus.Config
{
    /// <summary>
    /// Represents an extension element in the editor configuration for creating a context menu extension.
    /// </summary>
    public class WhereUsedPlusContextMenu : ContextMenuExtension
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public WhereUsedPlusContextMenu()
        {
            // This is the id which gets put on the html element for this menu (to target with css/js).
            AssignId = "WhereUsedPlus"; 
            // The name of the extension menu
            Name = "WhereUsedPlusMenu";
            // Where to add the new menu in the current context menu.
            InsertBefore = "cm_refresh";

            // Generate all of the context menu items...
            AddItem("cm_whereusedplus", "Where Used Plus", "WhereUsedPlus");

            // We need to addd our resource group as a dependency to this extension
            Dependencies.Add<WhereUsedPlusResourceGroup>();

            // Actually apply our extension to a particular view.  You can have multiple.
            Apply.ToView(Constants.Views.DashboardView);
        }
    }
}
