﻿<%@ Control Language="C#" %>
<%@ Import Namespace="Tridion.Web.UI" %>

<!-- This is an example of a drop down ribbon toolbar button. You assign the filename of this control in the RibbonToolbar's
     Group property. -->
<c:RibbonButton runat="server" CommandName="WhereUsedPlus" IsDropdownButton="false" Title="Where Used Plus" Label="Where Used Plus" ID="WhereUsedPlusBtn" />
